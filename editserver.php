<?php
require_once 'meekrodb.2.3.class.php';
DB::$user = 'root';
DB::$password = '';
DB::$dbName = 'christoffel';

session_start();

$sessionEmail = $_SESSION['email'];

$userid = $_SESSION['userid'];

$errors = array();

$db = mysqli_connect('localhost', 'root', '', 'christoffel')
or die('Error: '.mysqli_connect_error());

$voornaam = "";
$tussenvoegsel = "";
$achternaam = "";
$email = "";
$telNummer = "";
$wachtwoord = "";
$newWachtwoord1 = "";
$newWachtwoord2 = "";



$query = "SELECT * FROM gebruikers WHERE email = '$sessionEmail'";
$result = mysqli_query($db, $query)
    or die('Error '.mysqli_error($db).' with query '.$query);

if($result) {
    $gebruikerInfo = mysqli_fetch_assoc($result);
}

if (isset($_POST['submit'])) {

    print_r($gebruikerInfo);

    $voornaam = mysqli_real_escape_string($db, $_POST['voornaam']);
    $tussenvoegsel = mysqli_real_escape_string($db, $_POST['tussenvoegsel']);
    $achternaam = mysqli_real_escape_string($db, $_POST['achternaam']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $telNummer = mysqli_real_escape_string($db, $_POST['telnummer']);
    $wachtwoord = $_POST['wachtwoord'];
    $newWachtwoord1 = $_POST['newWachtwoord1'];
    $newWachtwoord2 = $_POST['newWachtwoord2'];

    if (empty($voornaam)) {
        array_push($errors, "Voornaam is verplicht");
    }
    if (empty($achternaam)) {
        array_push($errors, "Achternaam is verplicht");
    }
    if (empty($email)) {
        array_push($errors, "Email is verplicht");
    }
    if (empty($telNummer)) {
        array_push($errors, "Telefoonnummer is verplicht");
    }
    if (empty($wachtwoord)) {
        array_push($errors, "Wachtwoord is verplicht");
    }
    if (!password_verify($wachtwoord, $gebruikerInfo['wachtwoord'])) {
        array_push($errors, "Wachtwoord komt niet overeen met oude wachtwoord");
    }
    if (isset($newWachtwoord1) && isset($newWachtwoord2)) {
        if ($newWachtwoord1 != $newWachtwoord2) {
            array_push($errors, "De wachtwoorden komen niet overeen");
        } else {
            $wachtwoord = $newWachtwoord1;
        }
    } else {
        $wachtwoord = $wachtwoord;
    }


    $id = $gebruikerInfo['gebruiker_id'];

    if (count($errors) == 0) {
        $wachtwoord = password_hash($wachtwoord, PASSWORD_DEFAULT);

        DB::update('gebruikers', array(
            'voornaam' => $voornaam,
            'tussenvoegsel' => $tussenvoegsel,
            'achternaam' => $achternaam,
            'email' => $email,
            'telnummer' => $telNummer,
            'wachtwoord' => $wachtwoord
        ), "gebruiker_id=%i", $id);

        header('location: account.php');
    }
}
?>
