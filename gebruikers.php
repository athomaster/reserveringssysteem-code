<?php

include('delete.php');

$db = mysqli_connect('localhost', 'root', '', 'christoffel')
or die('Error: '.mysqli_connect_error());

$query = "SELECT * FROM gebruikers";

$result = mysqli_query($db, $query)
or die('Error '.mysqli_error($db).' with query '.$query);

$gebruikerInfo = [];

while($row = mysqli_fetch_assoc($result))
{
    $gebruikerInfo[] = $row;
}

mysqli_close($db);



if (!isset($_SESSION['email'])) {
    $_SESSION['msg'] = "U moet eerst inloggen";
    header('location: login.php');
}

if ($_SESSION['admin'] == 1) {
    $admin = 1;
} else {
    $admin = 0;
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gebruikers</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="CSS/delete.css" rel="stylesheet" type="text/css"/>
</head>
<header>
    <div class="logoBox">
        <img class="logo" src="pictures/logoschool_christoffel1.webp" alt="Christoffel">
    </div>

    <div class="menuBar">
        <div class="menuBar">
            <?php if ($admin == 1) { ?>
                <a href="register.php">REGISTREREN</a>
                <a href="reserveringen.php">RESERVERINGEN</a>
                <a id="active" href="gebruikers.php">GEBRUIKERS</a>
            <?php } ?>
            <a href="account.php">ACCOUNT</a>
            <a href="reserveren.php">RESERVEREN</a>
            <a href="uitloggen.php" class="">UITLOGGEN</a>
        </div>
    </div>
</header>
<body>
<div class="container">
    <div class="info">
        <?php
        foreach ($gebruikerInfo as $info) { ?>
        <div class="infoblock">
            <li>Voornaam: <?= $info['voornaam'] ?><li>
            <li>tussenvoegsel: <?= $info['tussenvoegsel']?></li>
            <li>achternaam: <?= $info['achternaam']?></li>
            <li>email: <?= $info['email']?></li>
            <li>telnummer: <?= $info['telnummer']?></li>
            <li>stichting: <?= $info['stichting']?></li>
            <li>admin: <?= $info['admin']?></li>
            <br/>
            <form method="post"  action="gebruikers.php">
                <input type="hidden" name="userid" value="<?= $gebruikerInfo['gebruiker_id'] ?>"/>
                <button type="submit" class="button" name="submit">Account verwijderen</button>
            </form>
        </div>
        <?php } ?>
    </div>
</div>
</body>
</html>