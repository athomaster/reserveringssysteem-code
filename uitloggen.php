<?php
    session_start();
    $_SESSION = array();
    session_destroy();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="CSS/form.css" rel="stylesheet" type="text/css"/>
    <title>Uitgelogd</title>
</head>
<header>
    <h1>U bent nu uitgelogd</h1>
</header>

<body>
    <div class="container">
        <h2>U bent nu uitgelogd</h2>
        <a href="index.php">Terug naar de home pagina</a>
    </div>
</body>
</html>
