<?php
require_once 'meekrodb.2.3.class.php';
DB::$user = 'root';
DB::$password = '';
DB::$dbName = 'christoffel';

session_start();

$errors = array();

$db = mysqli_connect('localhost', 'root', '', 'christoffel');

$voornaam = "";
$tussenvoegsel = "";
$achternaam = "";
$email    = "";
$telNummer = "";
$stichting = "";
$admin = "";


if (isset($_POST['reg_user'])) {
    $voornaam = mysqli_real_escape_string($db, $_POST['voornaam']);
    $tussenvoegsel = mysqli_real_escape_string($db, $_POST['tussenvoegsel']);
    $achternaam = mysqli_real_escape_string($db, $_POST['achternaam']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $telNummer = mysqli_real_escape_string($db, $_POST['telnummer']);
    $stichting = mysqli_real_escape_string($db, $_POST['stichting']);
    if (isset($_POST['admin'])) {
        $admin = 1;
    }else{
        $admin = 0;
    }
    $wachtwoord_1 = $_POST['wachtwoord_1'];
    $wachtwoord_2 = $_POST['wachtwoord_2'];

    if (empty($voornaam)) { array_push($errors, "Voornaam is verplicht"); }
    if (empty($achternaam)) { array_push($errors, "Achternaam is verplicht"); }
    if (empty($email)) { array_push($errors, "Email is verplicht"); }
    if (empty($telNummer)) { array_push($errors, "Telefoonnummer is verplicht"); }
    if (empty($stichting)) { array_push($errors, "Stichting is verplicht"); }
    if (empty($wachtwoord_1)) { array_push($errors, "Wachtwoord is verplicht"); }
    if ($wachtwoord_1 != $wachtwoord_2) {
        array_push($errors, "De wachtwoorden komen niet overeen");
    }

    $user_check = DB::query("SELECT * FROM gebruikers WHERE email='$email' LIMIT 1");
    foreach ($user_check as $row) {
        $user_email = "email: " . $row['email'] . "\n";
    }
    if ($row['email'] === $email) {
        array_push($errors, "Email is al geregistreerd");
    }

//Alle gegevens worden in de database gezet
    if (count($errors) == 0) {
        $wachtwoord = password_hash($wachtwoord_1, PASSWORD_DEFAULT);

        DB::insert('gebruikers', array(
            'voornaam' => $voornaam,
            'tussenvoegsel' => $tussenvoegsel,
            'achternaam' => $achternaam,
            'email' => $email,
            'telnummer' => $telNummer,
            'stichting' => $stichting,
            'admin' => $admin,
            'wachtwoord' => $wachtwoord
        ));

        $_SESSION['email'] = $email;
        if ($admin == 1){
            $_SESSION['admin'] = 1;
        }
        $_SESSION['success'] = "You are now logged in";
        header('location: index.php');
    }
}

    if (isset($_POST['login'])) {
        $email = mysqli_real_escape_string($db, $_POST['email']);
        $password = mysqli_real_escape_string($db, $_POST['password']);

        if (empty($email)) {
            array_push($errors, "Email is vereist");
        }
        if (empty($password)) {
            array_push($errors, "Wachtwoord is vereist");
        }

        if (count($errors) == 0) {
            $query = "SELECT * FROM gebruikers WHERE email='$email'";
            $results = mysqli_query($db, $query);
            if (mysqli_num_rows($results) == 1) {
                $account = mysqli_fetch_assoc($results);
                if (password_verify($password, $account['wachtwoord'])){
                    $_SESSION['email'] = $email;
                    if ($account['admin'] == 1) {
                        $_SESSION['admin'] = 1;
                    }
                    $_SESSION['userid'] = $account['gebruiker_id'];
                    $_SESSION['success'] = "U bent nu ingelogd";
                    header('location: index.php');
                }
            } else {
                array_push($errors, "Foute email/wachtwoord combinatie");
            }
        }
    }

?>