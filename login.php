<?php
    include('server.php')
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="CSS/form.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
</head>
<header>
    <div class="logoBox">
        <img class="logo" src="pictures/logoschool_christoffel1.webp" alt="Christoffel">
    </div>
    <h1></h1>

</header>
<body>
    <div class="container">
        <form action="login.php" method="post">
            <?php include('errors.php'); ?>
            <div class="input-group">
                <label for="email">E-mail adres: </label>
                <input id="email" type="text" name="email" value=""/>
            </div>
            <div class="input-group">
                <label for="password">Wachtwoord: </label>
                <input id="password" type="password" name="password" value=""/>
            </div>

            <div class="input-group">
                <input type="submit" name="login" value="Login"/>
            </div>
        </form>
    </div>


</body>
</html>
