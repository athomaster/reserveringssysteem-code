<?php
session_start();

$session = false;


if (isset($_SESSION['email'])) {
    $sessionEmail = $_SESSION['email'];
    $userid = $_SESSION['userid'];
    $admin = $_SESSION['admin'];
    $session = true;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reserveringssysteem</title>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
        font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
</head>
<header>
    <div class="logoBox">
        <img class="logo" src="pictures/logoschool_christoffel1.webp" alt="Christoffel">
    </div>

    <div class="menuBar">
        <div class="menuBar">
            <?php if (isset($userid)) { ?>
                <?php if ($admin == 1) { ?>
                    <a href="register.php">REGISTREREN</a>
                    <a href="reserveringen.php">RESERVERINGEN</a>
                    <a href="gebruikers.php">GEBRUIKERS</a>
                <?php } ?>
                <a href="account.php">ACCOUNT</a>
                <a href="reserveren.php">RESERVEREN</a>
                <a href="uitloggen.php" class="">UITLOGGEN</a>

            <?php } ?>
            <?php if ($session == false) { ?>
            <a class="login" href="login.php">INLOGGEN</a>
            <?php } ?>
        </div>
    </div>

</header>

<body>
    <div class="container">
        <p>Welkom</p>
    </div>

</body>
</html>