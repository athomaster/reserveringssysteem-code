<?php

include('server.php');

session_start();

if (!isset($_SESSION['email'])) {
    $_SESSION['msg'] = "U moet eerst inloggen";
    header('location: login.php');
}

if ($_SESSION['admin'] == 1) {
    $admin = 1;
} else {
    $admin = 0;
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Registreren</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="CSS/form.css" rel="stylesheet" type="text/css"/>
</head>

<header>
    <div class="logoBox">
        <img class="logo" src="pictures/logoschool_christoffel1.webp" alt="Christoffel">
    </div>

    <div class="menuBar">
        <div class="menuBar">
            <?php if ($admin == 1) { ?>
                <a id="active" href="register.php">REGISTREREN</a>
                <a href="reserveringen.php">RESERVERINGEN</a>
                <a href="gebruikers.php">GEBRUIKERS</a>
            <?php } ?>
            <a href="account.php">ACCOUNT</a>
            <a href="reserveren.php">RESERVEREN</a>
            <a href="uitloggen.php" class="">UITLOGGEN</a>
        </div>
    </div>
</header>

<body>

<div class="container">
    <form method="post" action="register.php" class="formbody">
        <?php include('errors.php'); ?>
        <p class="required">* is verplicht</p>
        <div class="input-group">
            <label for="voornaam">Voornaam *</label>
            <input id="voornaam" type="text" name="voornaam" value="<?php echo $voornaam; ?>" required>
        </div>
        <div class="input-group">
            <label for="tussenvoegsel">Tussenvoegsel</label>
            <input id="tussenvoegsel" type="text" name="tussenvoegsel" value="<?php echo $tussenvoegsel; ?>">
        </div>
        <div class="input-group">
            <label for="achternaam">Achternaam *</label>
            <input id="achternaam" type="text" name="achternaam" value="<?php echo $achternaam; ?>" required>
        </div>
        <div class="input-group">
            <label for="email">E-mail adres *</label>
            <input id="email" type="text" name="email" value="<?php echo $email; ?>" required>
        </div>
        <div class="input-group">
            <label for="telnummer">Telefoonnummer *</label>
            <input id="telnummer" type="text" name="telnummer" value="<?php echo $telNummer; ?>" required>
        </div>
        <div class="input-group">
            <label for="stichting">Stichting *</label>
            <input id="stichting" type="text" name="stichting" value="<?php echo $stichting; ?>" required>
        </div>
        <div class="input-group">
            <label for="admin">Admin</label>
            <input id="admin" type="checkbox" name="admin">
        </div>
        <div class="input-group">
            <label for="wachtwoord">Wachtwoord *</label>
            <input id="wachtwood" type="password" name="wachtwoord_1" value="" required>
        </div>
        <div class="input-group">
            <label for="wachtwoord">Wachtwoord bevestigen *</label>
            <input id="wachtwood" type="password" name="wachtwoord_2" value="" required>
        </div>
        <div class="input-group">
            <button type="submit" class="btn" name="reg_user">Register</button>
        </div>
    </div>
</form>
</body>
</html>