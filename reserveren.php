<?php

session_start();

if (!isset($_SESSION['email'])) {
    $_SESSION['msg'] = "U moet eerst inloggen";
    header('location: login.php');
}

if ($_SESSION['admin'] == 1) {
    $admin = 1;
} else {
    $admin = 0;
}

$_SESSION['userid'];

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Merriweather', serif;
        }

        header {
            font-family: 'Merriweather', serif;
        }
    </style>
    <title>Reserveren</title>

    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="CSS/form.css" rel="stylesheet" type="text/css"/>

</head>
<header>
    <div class="logoBox">
        <img class="logo" src="pictures/logoschool_christoffel1.webp" alt="Christoffel">
    </div>

    <div class="menuBar">
        <div class="menuBar">
            <?php if ($admin == 1) { ?>
                <a href="register.php">REGISTREREN</a>
                <a href="reserveringen.php">RESERVERINGEN</a>
                <a href="gebruikers.php">GEBRUIKERS</a>
            <?php } ?>
            <a href="account.php">ACCOUNT</a>
            <a id="active" href="reserveren.php">RESERVEREN</a>
            <a href="uitloggen.php" class="">UITLOGGEN</a>
        </div>
    </div>
</header>

<body>
    <div class="container">
        <form action="action.php">
            <div class="input-group">
                <label for="datum">Datum</label>
                <input type="date" name="datum" required>
            </div>
            <div class="input-group">
                <label for="tijd">Tijd</label>
                <input type="time" name="tijd" required>
            </div>

        </form>
    </div>
</body>
</html>
